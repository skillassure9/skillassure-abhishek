// LAB 2

public class Main
{
	public static void main(String[] args) {
	    

// goal of the pgm
// declare the variables

	    int mark1=0;
 	    int mark2=0;
 	    int mark3=0;
 	    int avg=0;
 	    int total=0;
// initialize the variables

	    mark1=35;
 	    mark2=35;
 	    mark3=35;
	   
// implement formula and do the calculations

            avg=(mark1+mark2+mark3)/3;
 	    total=mark1+mark2+mark3;

// apply condition and display the value	
 	 
 	    System.out.println("average:" + avg);
 	    System.out.println("total marks:"+ total);
	    
	    if(mark1>=35 && mark2>=35 && mark3>=35)
	    {
	        if(avg>=60)
		        System.out.println("first class");
		    else if(avg>=50 && avg<60)
		        System.out.println("second class");
		    else if(avg>=35 && avg<50)
		        System.out.println("pass class");
	    }
		else
		System.out.println("fail");

	} 
	
}